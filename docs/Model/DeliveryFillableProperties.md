# # DeliveryFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | [**\DateTime**](\DateTime.md) | желаемая дата доставки | [optional] 
**timeslot** | [**\Ensi\OmsClient\Dto\Timeslot**](Timeslot.md) |  | [optional] 
**status** | **int** | статус отправления из DeliveryStatusEnum | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


