# # OrderItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id элемента корзины | 
**order_id** | **int** | ID заказа | 
**shipment_id** | **int** | ID отгрузки | 
**offer_id** | **int** | ID оффера | 
**name** | **string** | Название товара | 
**qty** | **float** | Кол-во товара | 
**old_qty** | **float** | Старое кол-во товара | [optional] 
**price** | **int** | Цена товара (цена * qty - скидки) (в коп.) | 
**price_per_one** | **int** | Цена единичного товара (в коп.) | 
**cost** | **int** | Цена товара до скидок (цена * qty) (в коп.) | 
**cost_per_one** | **int** | Цена единичного товара до скидок (в коп.) | 
**product_weight** | **float** | Вес нетто товара (кг) | 
**product_weight_gross** | **float** | Вес брутто товара (кг) | 
**product_width** | **float** | Ширина товара (мм) | 
**product_height** | **float** | Высота товара (мм) | 
**product_length** | **float** | Длина товара (мм) | 
**product_barcode** | **string** | артикул (EAN) | 
**refund_qty** | **float** | Количество возвращаемых элементов корзины в заявке | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления | 
**is_added** | **bool** | флаг, что товар был добавлен | [optional] 
**is_deleted** | **bool** | флаг, что товар был удалён | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


