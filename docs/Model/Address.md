# # Address

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address_string** | **string** | Полная строка адреса | 
**country_code** | **string** |  | 
**post_index** | **string** | Почтовый индекс | 
**region** | **string** | Регион | 
**region_guid** | **string** | GUID региона | 
**area** | **string** | название области | [optional] 
**area_guid** | **string** | GUID области | [optional] 
**city** | **string** | название города | [optional] 
**city_guid** | **string** | GUID города | [optional] 
**street** | **string** | улица | [optional] 
**house** | **string** | дом | [optional] 
**block** | **string** | Строение / Корпус | [optional] 
**flat** | **string** | Квартира / Офис | [optional] 
**floor** | **string** | этаж | [optional] 
**porch** | **string** | подъезд | [optional] 
**intercom** | **string** | код домофона | [optional] 
**geo_lat** | **string** | широта | 
**geo_lon** | **string** | долгота | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


