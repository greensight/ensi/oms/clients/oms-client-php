# # RefundReason

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | идентификатор | 
**created_at** | [**\DateTime**](\DateTime.md) | дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления | 
**code** | **string** | Символьный код причины возврата | [optional] 
**name** | **string** | Название причины возврата | [optional] 
**description** | **string** | Детальное описание причины возврата | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


