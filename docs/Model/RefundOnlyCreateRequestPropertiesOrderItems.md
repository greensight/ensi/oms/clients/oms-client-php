# # RefundOnlyCreateRequestPropertiesOrderItems

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | идентификатор элемента корзины | [optional] 
**qty** | **float** | количество возвращаемых позиций | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


