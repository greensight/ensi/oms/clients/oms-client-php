# # DeliveryReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID отправления | 
**order_id** | **int** | ID заказа | 
**number** | **string** | номер отправления | 
**status_at** | [**\DateTime**](\DateTime.md) | дата установки статуса | 
**cost** | **int** | себестоимость доставки, полученная от службы доставки в копейках | 
**width** | **int** | ширина | 
**height** | **int** | высота | 
**length** | **int** | длина | 
**weight** | **int** | вес | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


