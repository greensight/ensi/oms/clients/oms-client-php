# # ShipmentIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**delivery** | [**\Ensi\OmsClient\Dto\Delivery**](Delivery.md) |  | [optional] 
**order_items** | [**\Ensi\OmsClient\Dto\OrderItem[]**](OrderItem.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


