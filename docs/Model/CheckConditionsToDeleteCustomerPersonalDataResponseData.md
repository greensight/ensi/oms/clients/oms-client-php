# # CheckConditionsToDeleteCustomerPersonalDataResponseData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**can_delete** | **bool** | Персональные данные покупателя можно удалить в OMS | [optional] 
**message** | **string** | Сообщение об возможной причине отказа удаления персональных данных | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


