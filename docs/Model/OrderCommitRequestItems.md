# # OrderCommitRequestItems

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offer_id** | **int** | Оффер, лежащий в корзине | 
**cost_per_one** | **int** | Сумма за единичный товар до скидок (в копейках) | 
**price_per_one** | **int** | Сумма за едининчный товар со всеми скидками (в копейках) | 
**qty** | **float** | Кол-во товара | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


