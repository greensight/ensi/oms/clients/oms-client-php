# # SettingReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID параметра | [optional] 
**code** | **string** | символьный код параметра | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | дата создания параметра | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления параметра | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


