# # OrderChangePaymentSystemRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_system** | **int** | Система оплаты из PaymentSystemEnum | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


