# # RefundFile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | идентификатор | 
**refund_id** | **int** | идентификатор заявки на возврат | 
**original_name** | **string** | Оригинальное название файла | 
**file** | [**\Ensi\OmsClient\Dto\File**](File.md) |  | 
**created_at** | [**\DateTime**](\DateTime.md) | дата загрузки файла | 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления файла | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


