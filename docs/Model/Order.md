# # Order

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID заказа | 
**number** | **string** | номер заказа | 
**customer_id** | **int** | id покупателя | 
**customer_email** | **string** | почта покупателя | 
**cost** | **int** | стоимость (без учета скидки) (рассчитывается автоматически) в коп. | 
**price** | **int** | стоимость (с учетом скидок) (рассчитывается автоматически) в коп. | 
**spent_bonus** | **int** | списано бонусов | 
**added_bonus** | **int** | начислено бонусов | 
**promo_code** | **string** | Использованный промокод | 
**status_at** | [**\DateTime**](\DateTime.md) | дата установки статуса заказа | 
**source** | **int** | источник заказа из OrderSourceEnum | 
**payment_status** | **int** | статус оплаты из PaymentStatusEnum | 
**payment_status_at** | [**\DateTime**](\DateTime.md) | дата установки статуса оплаты | 
**payed_at** | [**\DateTime**](\DateTime.md) | Дата оплаты | 
**payed_price** | **int** | Сумма оплаты | 
**payment_expires_at** | [**\DateTime**](\DateTime.md) | Дата, до которой нужно провести оплату | 
**payment_method** | **int** | метод оплаты из PaymentMethodEnum | 
**payment_system** | **int** |  | 
**payment_link** | **string** | Ссылка для оплаты во внещней системе | 
**payment_external_id** | **string** | ID оплаты во внешней системе | 
**payment_data** | [**\Ensi\OmsClient\Dto\OrderPaymentData**](OrderPaymentData.md) |  | 
**is_problem_at** | [**\DateTime**](\DateTime.md) | дата установки флага проблемного заказа | 
**is_editable** | **bool** | флаг, что заказ может быть изменён | [optional] 
**is_changed** | **bool** | флаг, что заказ был изменён | [optional] 
**is_expired** | **bool** | флаг, что заказ просроченный | 
**is_expired_at** | [**\DateTime**](\DateTime.md) | дата установки флага просроченного заказа | 
**is_return** | **bool** | флаг, что заказ возвращен | 
**is_return_at** | [**\DateTime**](\DateTime.md) | дата установки флага возвращенного заказа | 
**is_partial_return** | **bool** | флаг, что заказ частично возвращен | 
**is_partial_return_at** | [**\DateTime**](\DateTime.md) | дата установки флага частично возвращенного заказа | 
**created_at** | [**\DateTime**](\DateTime.md) | дата создания заказа | 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления заказа | 
**responsible_id** | **int** | Идентификатор ответственного за заказ | [optional] 
**status** | **int** | статус заказа из OrderStatus | [optional] 
**client_comment** | **string** | комментарий клиента | [optional] 
**receiver_name** | **string** | имя получателя | [optional] 
**receiver_phone** | **string** | телефон получателя | [optional] 
**receiver_email** | **string** | e-mail получателя | [optional] 
**is_problem** | **bool** | флаг, что заказ проблемный | [optional] 
**problem_comment** | **string** | последнее сообщение продавца о проблеме со сборкой | [optional] 
**delivery_service** | **int** | служба доставки | [optional] 
**delivery_method** | **int** | метод доставки | [optional] 
**delivery_cost** | **int** | стоимость доставки (без учета скидки) в копейках | [optional] 
**delivery_price** | **int** | стоимость доставки (с учетом скидки) в копейках | [optional] 
**delivery_point_id** | **int** | ID пункта самовывоза из сервиса логистики | [optional] 
**delivery_address** | [**\Ensi\OmsClient\Dto\Address**](Address.md) |  | [optional] 
**delivery_comment** | **string** | комментарий к доставке | [optional] 
**items** | [**\Ensi\OmsClient\Dto\OrderItem[]**](OrderItem.md) |  | [optional] 
**deliveries** | [**\Ensi\OmsClient\Dto\Delivery[]**](Delivery.md) |  | [optional] 
**files** | [**\Ensi\OmsClient\Dto\OrderFile[]**](OrderFile.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


