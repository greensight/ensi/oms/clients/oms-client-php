# # OrderCommitRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer_id** | **int** | Пользователь, который оформляет заказ | 
**customer_email** | **string** | Почта пользователя, который оформляет заказ | 
**client_comment** | **string** | Комментарий клиента к заказу | 
**payment_system_id** | **int** |  | 
**source** | **int** |  | 
**promo_code** | **string** | Использованный промокод | 
**delivery_method** | **int** | метод доставки из логистики | 
**delivery_address** | [**\Ensi\OmsClient\Dto\Address**](Address.md) |  | 
**delivery_comment** | **string** | Комментарий клиента к доставке | 
**delivery_price** | **int** | Стоимость доставки со скидкой (в копейках) | [optional] 
**receiver_name** | **string** | Получатель. ФИО | 
**receiver_phone** | **string** | Получатель. Телефон | 
**receiver_email** | **string** | Получатель. Почта | 
**deliveries** | [**\Ensi\OmsClient\Dto\OrderCommitRequestDeliveries[]**](OrderCommitRequestDeliveries.md) | Информация об отправлениях | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


