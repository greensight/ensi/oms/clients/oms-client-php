# # OrderIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**\Ensi\OmsClient\Dto\OrderItem[]**](OrderItem.md) |  | [optional] 
**deliveries** | [**\Ensi\OmsClient\Dto\Delivery[]**](Delivery.md) |  | [optional] 
**files** | [**\Ensi\OmsClient\Dto\OrderFile[]**](OrderFile.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


