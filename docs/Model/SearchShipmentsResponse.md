# # SearchShipmentsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\OmsClient\Dto\Shipment[]**](Shipment.md) |  | 
**meta** | [**\Ensi\OmsClient\Dto\SearchDeliveriesResponseMeta**](SearchDeliveriesResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


