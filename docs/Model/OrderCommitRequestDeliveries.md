# # OrderCommitRequestDeliveries

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipments** | [**\Ensi\OmsClient\Dto\OrderCommitRequestShipments[]**](OrderCommitRequestShipments.md) | Отгрузки, входящие в отправление | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


