# # OrderItemsAddRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_items** | [**\Ensi\OmsClient\Dto\OrderItemsAddRequestOrderItems[]**](OrderItemsAddRequestOrderItems.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


