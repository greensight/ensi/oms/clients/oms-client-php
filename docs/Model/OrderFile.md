# # OrderFile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID файла | 
**order_id** | **int** | ID заказа | 
**original_name** | **string** | Оригинальное название файла | 
**file** | [**\Ensi\OmsClient\Dto\File**](File.md) |  | 
**created_at** | [**\DateTime**](\DateTime.md) | дата создания файла | 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления файла | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


