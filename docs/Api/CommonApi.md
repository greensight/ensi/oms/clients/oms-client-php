# Ensi\OmsClient\CommonApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**checkConditionsToDeleteCustomerPersonalData**](CommonApi.md#checkConditionsToDeleteCustomerPersonalData) | **POST** /common/customer-personal-data:check-conditions-to-delete | Проверка условий в OMS на удаление персональных данных покупателя
[**deleteCustomerPersonalData**](CommonApi.md#deleteCustomerPersonalData) | **POST** /common/customer-personal-data:delete | Удаление персональных данных покупателя в OMS
[**patchSettings**](CommonApi.md#patchSettings) | **PATCH** /common/settings | Обновление параметров
[**searchSettings**](CommonApi.md#searchSettings) | **GET** /common/settings | Получение всех параметров



## checkConditionsToDeleteCustomerPersonalData

> \Ensi\OmsClient\Dto\CheckConditionsToDeleteCustomerPersonalDataResponse checkConditionsToDeleteCustomerPersonalData($check_conditions_to_delete_customer_personal_data_request)

Проверка условий в OMS на удаление персональных данных покупателя

Проверка условий в OMS на удаление персональных данных покупателя

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\CommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$check_conditions_to_delete_customer_personal_data_request = new \Ensi\OmsClient\Dto\CheckConditionsToDeleteCustomerPersonalDataRequest(); // \Ensi\OmsClient\Dto\CheckConditionsToDeleteCustomerPersonalDataRequest | 

try {
    $result = $apiInstance->checkConditionsToDeleteCustomerPersonalData($check_conditions_to_delete_customer_personal_data_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->checkConditionsToDeleteCustomerPersonalData: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **check_conditions_to_delete_customer_personal_data_request** | [**\Ensi\OmsClient\Dto\CheckConditionsToDeleteCustomerPersonalDataRequest**](../Model/CheckConditionsToDeleteCustomerPersonalDataRequest.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\CheckConditionsToDeleteCustomerPersonalDataResponse**](../Model/CheckConditionsToDeleteCustomerPersonalDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteCustomerPersonalData

> \Ensi\OmsClient\Dto\EmptyDataResponse deleteCustomerPersonalData($delete_customer_personal_data_request)

Удаление персональных данных покупателя в OMS

Удаление персональных данных покупателя в OMS

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\CommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$delete_customer_personal_data_request = new \Ensi\OmsClient\Dto\DeleteCustomerPersonalDataRequest(); // \Ensi\OmsClient\Dto\DeleteCustomerPersonalDataRequest | 

try {
    $result = $apiInstance->deleteCustomerPersonalData($delete_customer_personal_data_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->deleteCustomerPersonalData: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delete_customer_personal_data_request** | [**\Ensi\OmsClient\Dto\DeleteCustomerPersonalDataRequest**](../Model/DeleteCustomerPersonalDataRequest.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchSettings

> \Ensi\OmsClient\Dto\SettingsResponse patchSettings($patch_several_settings_request)

Обновление параметров

Обновление параметров

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\CommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$patch_several_settings_request = new \Ensi\OmsClient\Dto\PatchSeveralSettingsRequest(); // \Ensi\OmsClient\Dto\PatchSeveralSettingsRequest | 

try {
    $result = $apiInstance->patchSettings($patch_several_settings_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->patchSettings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patch_several_settings_request** | [**\Ensi\OmsClient\Dto\PatchSeveralSettingsRequest**](../Model/PatchSeveralSettingsRequest.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\SettingsResponse**](../Model/SettingsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchSettings

> \Ensi\OmsClient\Dto\SettingsResponse searchSettings()

Получение всех параметров

Получение всех параметров

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\CommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->searchSettings();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->searchSettings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\OmsClient\Dto\SettingsResponse**](../Model/SettingsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

