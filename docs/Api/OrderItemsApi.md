# Ensi\OmsClient\OrderItemsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addOrderItems**](OrderItemsApi.md#addOrderItems) | **POST** /orders/orders/{id}:add-order-items | Добавление OrderItems в заказ
[**changeOrderItemQty**](OrderItemsApi.md#changeOrderItemQty) | **POST** /orders/orders/{id}:change-order-items-qty | Изменение количества OrderItem
[**deleteOrderItems**](OrderItemsApi.md#deleteOrderItems) | **POST** /orders/orders/{id}:delete-order-items | Удаление OrderItems из заказа



## addOrderItems

> \Ensi\OmsClient\Dto\OrderItemsAddResponse addOrderItems($id, $order_items_add_request)

Добавление OrderItems в заказ

Добавление OrderItems в заказ

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\OrderItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$order_items_add_request = new \Ensi\OmsClient\Dto\OrderItemsAddRequest(); // \Ensi\OmsClient\Dto\OrderItemsAddRequest | 

try {
    $result = $apiInstance->addOrderItems($id, $order_items_add_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderItemsApi->addOrderItems: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **order_items_add_request** | [**\Ensi\OmsClient\Dto\OrderItemsAddRequest**](../Model/OrderItemsAddRequest.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\OrderItemsAddResponse**](../Model/OrderItemsAddResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## changeOrderItemQty

> \Ensi\OmsClient\Dto\OrderItemChangeQtyResponse changeOrderItemQty($id, $order_item_change_qty_request)

Изменение количества OrderItem

Изменение количества OrderItem

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\OrderItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$order_item_change_qty_request = new \Ensi\OmsClient\Dto\OrderItemChangeQtyRequest(); // \Ensi\OmsClient\Dto\OrderItemChangeQtyRequest | 

try {
    $result = $apiInstance->changeOrderItemQty($id, $order_item_change_qty_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderItemsApi->changeOrderItemQty: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **order_item_change_qty_request** | [**\Ensi\OmsClient\Dto\OrderItemChangeQtyRequest**](../Model/OrderItemChangeQtyRequest.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\OrderItemChangeQtyResponse**](../Model/OrderItemChangeQtyResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteOrderItems

> \Ensi\OmsClient\Dto\EmptyDataResponse deleteOrderItems($id, $order_items_delete_request)

Удаление OrderItems из заказа

Удаление OrderItems из заказа

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\OrderItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$order_items_delete_request = new \Ensi\OmsClient\Dto\OrderItemsDeleteRequest(); // \Ensi\OmsClient\Dto\OrderItemsDeleteRequest | 

try {
    $result = $apiInstance->deleteOrderItems($id, $order_items_delete_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderItemsApi->deleteOrderItems: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **order_items_delete_request** | [**\Ensi\OmsClient\Dto\OrderItemsDeleteRequest**](../Model/OrderItemsDeleteRequest.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

