# Ensi\OmsClient\RefundsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**attachRefundFile**](RefundsApi.md#attachRefundFile) | **POST** /refunds/refunds/{id}:attach-file | Загрузка вложения для заявки на возврат
[**createRefund**](RefundsApi.md#createRefund) | **POST** /refunds/refunds | Создание объекта типа Refund
[**deleteRefundFiles**](RefundsApi.md#deleteRefundFiles) | **DELETE** /refunds/refunds/{id}:delete-files | Удаление вложений для заявки на возврат
[**getRefund**](RefundsApi.md#getRefund) | **GET** /refunds/refunds/{id} | Получение объекта типа Refund
[**patchRefund**](RefundsApi.md#patchRefund) | **PATCH** /refunds/refunds/{id} | Обновление части полей объекта типа Refund
[**searchRefunds**](RefundsApi.md#searchRefunds) | **POST** /refunds/refunds:search | Поиск объектов типа Refund



## attachRefundFile

> \Ensi\OmsClient\Dto\RefundFileResponse attachRefundFile($id, $file)

Загрузка вложения для заявки на возврат

Загрузка вложения для заявки на возврат

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\RefundsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$file = "/path/to/file.txt"; // \SplFileObject | Загружаемый файл

try {
    $result = $apiInstance->attachRefundFile($id, $file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RefundsApi->attachRefundFile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **file** | **\SplFileObject****\SplFileObject**| Загружаемый файл | [optional]

### Return type

[**\Ensi\OmsClient\Dto\RefundFileResponse**](../Model/RefundFileResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createRefund

> \Ensi\OmsClient\Dto\RefundResponse createRefund($create_refund_request)

Создание объекта типа Refund

Создание объекта типа Refund

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\RefundsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_refund_request = new \Ensi\OmsClient\Dto\CreateRefundRequest(); // \Ensi\OmsClient\Dto\CreateRefundRequest | 

try {
    $result = $apiInstance->createRefund($create_refund_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RefundsApi->createRefund: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_refund_request** | [**\Ensi\OmsClient\Dto\CreateRefundRequest**](../Model/CreateRefundRequest.md)|  | [optional]

### Return type

[**\Ensi\OmsClient\Dto\RefundResponse**](../Model/RefundResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteRefundFiles

> \Ensi\OmsClient\Dto\EmptyDataResponse deleteRefundFiles($id, $delete_refund_files_request)

Удаление вложений для заявки на возврат

Удаление вложений для заявки на возврат

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\RefundsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$delete_refund_files_request = new \Ensi\OmsClient\Dto\DeleteRefundFilesRequest(); // \Ensi\OmsClient\Dto\DeleteRefundFilesRequest | 

try {
    $result = $apiInstance->deleteRefundFiles($id, $delete_refund_files_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RefundsApi->deleteRefundFiles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **delete_refund_files_request** | [**\Ensi\OmsClient\Dto\DeleteRefundFilesRequest**](../Model/DeleteRefundFilesRequest.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getRefund

> \Ensi\OmsClient\Dto\RefundResponse getRefund($id, $include)

Получение объекта типа Refund

Получение объекта типа Refund

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\RefundsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getRefund($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RefundsApi->getRefund: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\OmsClient\Dto\RefundResponse**](../Model/RefundResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchRefund

> \Ensi\OmsClient\Dto\RefundResponse patchRefund($id, $patch_refund_request)

Обновление части полей объекта типа Refund

Обновление части полей объекта типа Refund

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\RefundsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_refund_request = new \Ensi\OmsClient\Dto\PatchRefundRequest(); // \Ensi\OmsClient\Dto\PatchRefundRequest | 

try {
    $result = $apiInstance->patchRefund($id, $patch_refund_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RefundsApi->patchRefund: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_refund_request** | [**\Ensi\OmsClient\Dto\PatchRefundRequest**](../Model/PatchRefundRequest.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\RefundResponse**](../Model/RefundResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchRefunds

> \Ensi\OmsClient\Dto\SearchRefundsResponse searchRefunds($search_refunds_request)

Поиск объектов типа Refund

Поиск объектов типа Refund

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\RefundsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_refunds_request = new \Ensi\OmsClient\Dto\SearchRefundsRequest(); // \Ensi\OmsClient\Dto\SearchRefundsRequest | 

try {
    $result = $apiInstance->searchRefunds($search_refunds_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RefundsApi->searchRefunds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_refunds_request** | [**\Ensi\OmsClient\Dto\SearchRefundsRequest**](../Model/SearchRefundsRequest.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\SearchRefundsResponse**](../Model/SearchRefundsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

