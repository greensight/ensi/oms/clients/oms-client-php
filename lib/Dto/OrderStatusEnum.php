<?php
/**
 * OrderStatusEnum
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\OmsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * OMS
 *
 * Управление заказами
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Ensi\OmsClient\Dto;
use \Ensi\OmsClient\ObjectSerializer;

/**
 * OrderStatusEnum Class Doc Comment
 *
 * @category Class
 * @description Статус заказа. Расшифровка значений:   * &#x60;1&#x60; - Ожидает оплаты   * &#x60;10&#x60; - Новый   * &#x60;20&#x60; - Подтвержден   * &#x60;100&#x60; - Завершён   * &#x60;200&#x60; - Отменен
 * @package  Ensi\OmsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class OrderStatusEnum
{
    
    /** Ожидает оплаты */
    public const WAIT_PAY = 1;
    
    /** Новый */
    public const NEW = 10;
    
    /** Подтвержден */
    public const APPROVED = 20;
    
    /** Завершён */
    public const DONE = 100;
    
    /** Отменен */
    public const CANCELED = 200;
    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues(): array
    {
        return [
            self::WAIT_PAY,
            self::NEW,
            self::APPROVED,
            self::DONE,
            self::CANCELED,
        ];
    }

    /**
    * Gets allowable values and titles of the enum
    * @return string[]
    */
    public static function getDescriptions(): array
    {
        return [
            self::WAIT_PAY => 'Ожидает оплаты',
            self::NEW => 'Новый',
            self::APPROVED => 'Подтвержден',
            self::DONE => 'Завершён',
            self::CANCELED => 'Отменен',
        ];
    }
}


