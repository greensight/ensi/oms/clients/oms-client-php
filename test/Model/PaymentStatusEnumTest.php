<?php
/**
 * PaymentStatusEnumTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\OmsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * OMS
 *
 * Управление заказами
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace Ensi\OmsClient;

use PHPUnit\Framework\TestCase;

/**
 * PaymentStatusEnumTest Class Doc Comment
 *
 * @category    Class
 * @description Статус оплаты. Расшифровка значений:   * &#x60;1&#x60; - Не оплачен   * &#x60;2&#x60; - Средства захолдированы   * &#x60;100&#x60; - Оплачен   * &#x60;101&#x60; - Возврат   * &#x60;200&#x60; - Просрочен   * &#x60;201&#x60; - Отменен
 * @package     Ensi\OmsClient
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class PaymentStatusEnumTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "PaymentStatusEnum"
     */
    public function testPaymentStatusEnum()
    {
    }
}
